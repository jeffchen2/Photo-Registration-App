import React, { useState, useEffect } from 'react';
import { getUrl } from '@aws-amplify/storage';
import awsConfig from './aws-exports';

const PersonItem = ({ person, handlePersonUpdate, handlePersonDelete }) => {
  const [imageUrl, setImageUrl] = useState(null);

  useEffect(() => {
    const fetchImageUrl = async () => {
      if (person.photoUrl) {
        try {
          const linkToStorageFile = await getUrl({
            path: person.photoUrl,
            options: {
              validateObjectExistence: false,
              expiresIn: 3600, // Set the desired expiration time (in seconds)
            },
          });
          console.log('Generated URL:', linkToStorageFile.url);
          setImageUrl(linkToStorageFile.url);
        } catch (error) {
          console.error('Error fetching image:', error);
        }
      }
    };

    fetchImageUrl();
  }, [person.photoUrl]);

 return (
<tr>
  <td style={{ fontSize: '20px', width: '40%', padding: '12px 40px 12px 20px' }}>{person.name}</td>
  <td style={{ fontSize: '20px', padding: '12px 40px' }}>{person.location}</td>
  <td style={{ textAlign: 'center', padding: '12px 40px' }}>
    {imageUrl && (
      <img
        src={imageUrl}
        alt={person.name}
        style={{ maxWidth: '150px', maxHeight: '150px' }}
      />
    )}
  </td>
  <td style={{ textAlign: 'center', fontSize: '20px', padding: '12px 40px' }}>{person.gender}</td>
  <td style={{ textAlign: 'center', fontSize: '20px', padding: '12px 40px' }}>
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <button style={{ fontSize: '18px', marginRight: '10px' }} onClick={() => handlePersonUpdate(person)}>Edit</button>
      <button style={{ fontSize: '18px' }} onClick={() => handlePersonDelete(person.id)}>Delete</button>
    </div>
  </td>
</tr>
  );
};


export default PersonItem;