import React, { useEffect, useState } from 'react';
import './App.css';
import { withAuthenticator } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';
import { Amplify } from 'aws-amplify';
import { uploadData, getUrl, remove } from '@aws-amplify/storage';
import config from './aws-exports';
import { createPerson, deletePerson, updatePerson } from './graphql/mutations';
import { listPeople, getPerson } from './graphql/queries';
import { generateClient } from 'aws-amplify/api';
import PersonItem from './PersonItem';
import awsLogo from './aws.png';

Amplify.configure(config);
const client = generateClient({ authMode: 'userPool' });

function App() {
  const [editingPerson, setEditingPerson] = useState(null);
  const [personData, setPersonData] = useState([]);
  const [isEditing, setIsEditing] = useState(false);
  const [personImage, setPersonImage] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [imageUrls, setImageUrls] = useState(new Map());
  const [sortOrder, setSortOrder] = useState('asc');
  const [sortCriteria, setSortCriteria] = useState('name');
  const [selectedSort, setSelectedSort] = useState('');
  const itemsPerPage = 6;

  const handlePersonUpdate = async (person) => {
    setEditingPerson(person);
    setIsEditing(true);
  };

  useEffect(() => {
    const fetchPeople = async () => {
      try {
        const res = await client.graphql({ query: listPeople });
        const people = res.data.listPeople.items;
        setPersonData(people);
      } catch (error) {
        console.error('Error listing Person:', error);
      }
    };

    fetchPeople();
    generatePresignedUrls();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const personName = e.target.personName.value;
    const personLocation = e.target.personLocation.value;
    const personGender = e.target.gender.value;

    try {
      let imageUrl = '';
      if (personImage) {
        const timestamp = new Date().getTime();
        const uniqueFilePath = `picture-submission/${timestamp}.png`;
        const uploadedPhoto = await uploadData({
          path: uniqueFilePath,
          data: personImage,
        });
        imageUrl = uniqueFilePath;
      }

      if (editingPerson) {
        // Update person
        const { data } = await client.graphql({
          query: updatePerson,
          variables: {
            input: {
              id: editingPerson.id,
              name: personName,
              location: personLocation,
              gender: personGender,
              photoUrl: imageUrl || editingPerson.photoUrl,
            },
          },
        });
        setPersonData((currPersonList) =>
          currPersonList.map((p) => (p.id === editingPerson.id ? data.updatePerson : p))
        );
        setImageUrls(new Map(imageUrls.set(data.updatePerson.id, data.updatePerson.photoUrl)));
        setEditingPerson(null);
        setIsEditing(false);
        setPersonImage(null);
      } else {
        // Create person
        const { data } = await client.graphql({
          query: createPerson,
          variables: {
            input: {
              name: personName,
              location: personLocation,
              gender: personGender,
              photoUrl: imageUrl,
            },
          },
        });
        setPersonData((currPersonList) => [...currPersonList, data.createPerson]);
        setImageUrls(new Map(imageUrls.set(data.createPerson.id, data.createPerson.photoUrl)));
        setPersonImage(null);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const handlePersonDelete = async (personID) => {
    const newPeopleList = personData.filter((person) => person.id !== personID);
    try {
      // Fetch the person data using the getPerson query
      const { data } = await client.graphql({
        query: getPerson,
        variables: {
          id: personID,
        },
      });

      // Get the photoUrl from the fetched person data
      const personToDelete = data.getPerson;
      const photoUrl = personToDelete.photoUrl;

      // Delete the person's photo from S3 bucket if photoUrl exists
      if (photoUrl) {
        await remove({
          path: photoUrl,
        });
      }

      // Delete the person from the database
      await client.graphql({
        query: deletePerson,
        variables: {
          input: {
            id: personID,
          }, },
        });
  
        const updatedPersonData = newPeopleList;
        setPersonData(updatedPersonData);
        setImageUrls(new Map(Array.from(imageUrls.entries()).filter(([id]) => id !== personID)));
  
        // If the last entry on the current page is deleted, move to the previous page if not on the first page
        const totalPages = Math.ceil(updatedPersonData.length / itemsPerPage);
        if (currentPage > totalPages && currentPage > 1) {
          setCurrentPage(currentPage - 1);
        }
      } catch (error) {
        console.log('Error deleting person:', error);
      }
    };
  
    const handleImageUpload = (event) => {
      setPersonImage(event.target.files[0]);
    };
  
    const handleSortChange = (e) => {
      setSelectedSort(e.target.value);
      const [criteria, order] = e.target.value.split('-');
      handleSort(criteria, order);
    };
  
    const handleSort = (criteria, order) => {
      setSortCriteria(criteria);
      setSortOrder(order === 'asc' ? 'asc' : 'desc');
    };
  
    const sortedPersonData = [...personData].sort((a, b) => {
      let valueA, valueB;
      switch (sortCriteria) {
        case 'name':
          valueA = a.name.toLowerCase();
          valueB = b.name.toLowerCase();
          break;
        case 'location':
          valueA = a.location.toLowerCase();
          valueB = b.location.toLowerCase();
          break;
        case 'gender':
          valueA = a.gender.toLowerCase();
          valueB = b.gender.toLowerCase();
          break;
        default:
          valueA = a.name.toLowerCase();
          valueB = b.name.toLowerCase();
      }
  
      if (valueA < valueB) {
        return sortOrder === 'asc' ? -1 : 1;
      }
      if (valueA > valueB) {
        return sortOrder === 'asc' ? 1 : -1;
      }
      return 0;
    });
  
    const indexOfLastPerson = currentPage * itemsPerPage;
    const indexOfFirstPerson = indexOfLastPerson - itemsPerPage;
    const currentPersons = sortedPersonData.slice(indexOfFirstPerson, indexOfLastPerson);
  
    // Change page
    const paginate = (pageNumber) => {
      const totalPages = Math.ceil(personData.length / itemsPerPage);
      if (pageNumber >= 1 && pageNumber <= totalPages) {
        setCurrentPage(pageNumber);
      }
    };
  
    // Render pagination
    const renderPagination = () => {
      const pageNumbers = [];
      const totalPages = Math.ceil(personData.length / itemsPerPage);
  
      pageNumbers.push(
        <button
          key="prev"
          onClick={() => paginate(currentPage - 1)}
          disabled={currentPage === 1}
          style={{ marginRight: '5px' }}
        >
          Previous
        </button>
      );
  
      for (let i = 1; i <= totalPages; i++) {
        pageNumbers.push(
          <button
            key={i}
            className={currentPage === i ? 'active' : ''}
            onClick={() => paginate(i)}
            style={{ marginRight: '5px' }}
          >
            {i}
          </button>
        );
      }
  
      pageNumbers.push(
        <button
          key="next"
          onClick={() => paginate(currentPage + 1)}
          disabled={currentPage === totalPages}
          style={{ marginLeft: '5px' }}
        >
          Next
        </button>
      );
  
      return <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '20px' }}>{pageNumbers}</div>;
    };
  
    const generatePresignedUrls = async () => {
      const newImageUrls = new Map();
  
      for (const person of personData) {
        if (person.photoUrl) {
          try {
            const linkToStorageFile = await getUrl({
              path: person.photoUrl,
              options: {
                validateObjectExistence: false,
                expiresIn: 3600, // Set the desired expiration time (in seconds)
              },
            });
            newImageUrls.set(person.id, linkToStorageFile.url);
          } catch (error) {
            console.error('Error fetching image:', error);
          }
        }
      }
  
      setImageUrls(newImageUrls);
    };
  
    return (
      <div className="container">
        <header>
          <h1>Voter Registration System</h1>
        </header>
        <div style={{ textAlign: 'center'}}>
          <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '10px' }}>
            <form onSubmit={handleSubmit}>
              <input placeholder="Enter a name" name="personName" style={{ marginRight: '20px', width: '200px' }} />
              <input placeholder="Enter their location" name="personLocation" style={{ marginRight: '20px', width: '200px' }} />
              <select name="gender" style={{ marginRight: '20px' }}>
                <option value="none" disabled>
                  Select Gender
                </option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Other">Other</option>
              </select>
              <input type="file" onChange={handleImageUpload} style={{ marginRight: '20px' }} />
              <button type="submit" className="primary">{isEditing ? 'Edit Person' : 'Enter Person'}</button>
            </form>
          </div>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginBottom: '10px' }}>
            <h3>Page {currentPage}</h3>
            <select value={selectedSort} onChange={handleSortChange} style={{ fontSize: '16px' }}>
              <option value="">Sort by</option>
              <option value="name-asc">Name (A-Z)</option>
              <option value="name-desc">Name (Z-A)</option>
              <option value="location-asc">Location (A-Z)</option>
              <option value="location-desc">Location (Z-A)</option>
              <option value="gender-asc">Gender (Male/Female/Other)</option>
              <option value="gender-desc">Gender (Other/Female/Male)</option>
            </select>
          </div>
        </div>
        <main>
          <div>
            <table>
              <thead>
                <tr>
                  <th
                    style={{ textAlign: 'left', padding: '12px 40px 12px 20px', fontSize: '20px', width: '40%', cursor: 'pointer' }}
                    onClick={() => handleSort('name', sortOrder === 'asc' ? 'desc' : 'asc')}
                  >
                    Name
                  </th>
                  <th
                    style={{ textAlign: 'left', padding: '12px 40px', fontSize: '20px', cursor: 'pointer' }}
                    onClick={() => handleSort('location', sortOrder === 'asc' ? 'desc' : 'asc')}
                  >
                    Location
                  </th>
                  <th style={{ textAlign: 'center', padding: '12px 40px', fontSize: '20px' }}>Photo</th>
                  <th
                    style={{ textAlign: 'center', padding: '12px 40px', fontSize: '20px', cursor: 'pointer' }}
                    onClick={() => handleSort('gender', sortOrder === 'asc' ? 'desc' : 'asc')}
                    >
                      Gender
                    </th>
                    <th style={{ textAlign: 'center', padding: '12px 40px', fontSize: '20px' }}>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {currentPersons.map((person) => (
                    <PersonItem
                      key={person.id}
                      person={person}
                      imageUrl={imageUrls.get(person.id)}
                      handlePersonUpdate={handlePersonUpdate}
                      handlePersonDelete={handlePersonDelete}
                    />
                  ))}
                </tbody>
              </table>
            </div>
            <div>
            <div style={{marginBottom: '10px' }}></div>
              {renderPagination()}
            </div>
          </main>
          <footer>
            <div>
              <img src={awsLogo} alt="AWS Logo" />
            </div>
          </footer>
        </div>
      );
    }
    
    export default withAuthenticator(App);